<?php 
/**
 * Hero block
 * @author BarrelNY
 */ 

$page = get_page_by_path( 'front-page' ); 
$page__header = get_field('index__headline', $page);
$page__subheader = apply_filters('the_content', $page->post_content); 
$page__product = get_field('index__product', $page);
$page__image = add_inline_gradient(featured_image_or_fallback($page));

$latest = new WP_Query( array(
	'post_type' => 'post',
	'posts_per_page' => 1
)); 
$latest__image = featured_image_or_fallback($latest);
$latest__image_position = image_custom_position($latest);
$latest__icon = get_format_icon($latest); 
$latest__cta = get_cta($latest); ?>


<hero class="hero">
	<main class="hero__main" style="<?php echo $page__image; ?>">
		<div class="container">
			<div class="hero__copy">
				<span>
					<h1 class="hero__copy--title"><?php echo $page__header; ?></h1>
					<p class="hero__copy--subtitle"><?php echo $page__subheader; ?></p>
				</span>
			</div>
			<div class="hero__product">
				<span title="<?php echo $page__product['title']; ?>" alt="<?php echo $page__product['alt']; ?>" style="background-image:url('<?php echo $page__product['url']; ?>');">
				</span>
			</div>
		</div>
	</main>
	<section class="hero__latest">
		<?php 
		if ( $latest->have_posts() ) :
		    while ( $latest->have_posts() ) : $latest->the_post(); ?>
				<post class="post post--latest">
					<a href="<?php the_permalink(); ?>" class="post__image post__image--latest post--link">
						<span style="background-image:url('<?php echo $latest__image; ?>');background-position:<?php echo $latest__image_position; ?>">
						</span>
					</a>
					<div class="post__meta post__meta--latest">
						<div class="post__meta--icon">
							<?php echo $latest__icon; ?>
						</div>
						<a href="<?php echo get_month_link(get_the_date('Y'), get_the_date('m')); ?>" class="post__meta--date">
							<button><?php echo get_the_date('F d'); ?></button>
						</a>
						<a href="<?php the_permalink(); ?>" class="post__meta--title post--link">
							<h3><?php the_title(); ?></h3>
						</a>
						<div class="post__meta--description">
							<p><?php echo the_truncated_excerpt( get_the_content(), 36 ); ?></p>
						</div>
						<a href="<?php the_permalink(); ?>" class="post__meta--cta post--link">
							<?php echo $latest__cta; ?>
						</a>
					</div>
				</post>
			<?php endwhile; ?>
		<?php endif; ?>
		<?php wp_reset_postdata(); ?>
	</section>
</hero>

